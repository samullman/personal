const path = require(`path`)

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions

  const homePage = path.resolve(`src/templates/home.js`)
  const aboutPage = path.resolve(`src/templates/about.js`)
  const portfolioPage = path.resolve(`src/templates/portfolio.js`)

  createPage({
    path: "/",
    component: homePage,
    context: {}, // additional data can be passed via context
  })

  createPage({
    path: "/portfolio",
    component: portfolioPage,
    context: {}, // additional data can be passed via context
  })

  createPage({
    path: "/about",
    component: aboutPage,
    context: {}, // additional data can be passed via context
  })
  // const result = await graphql(`
  //   {
  //       allMarkdownRemark(filter: {frontmatter: {templateKey: home-page}}) {
  //       nodes {
  //           frontmatter {
  //           items {
  //               image
  //               link
  //               title
  //           }
  //           }
  //       }
  //       }
  //   }
  // `)

  // // Handle errors
  // if (result.errors) {
  //   reporter.panicOnBuild(`Error while running GraphQL query.`)
  //   return
  // }

  // result.data.allMarkdownRemark.nodes.forEach(({ node }) => {
    
  // })
}
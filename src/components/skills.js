import React from "react"
import styled from "styled-components"
import { StaticQuery, graphql  } from "gatsby"

const MySkills = styled.div`
  padding: 28px 0px;
  max-width: 1048px;
  display: block;
  margin: 0 auto;
  text-align: center;

  @media screen and (max-width: 768px) {
    padding: 48px 0px;
    overflow: auto;
    .column {
      width: 25%;
      float: left;
    }
  }

  @media screen and (max-width: 468px) {
    .column {
      width: 33%;
      float: left;
    }
  }
`


export default () => (
    <StaticQuery
      query={graphql`
      {
        allMarkdownRemark(filter: {frontmatter: {templateKey: {eq: "about-page"}}}) {
          nodes {
            frontmatter {
              skills {
                image
                link
                title
              }
            }
          }
        }
      }
      `}
      render={data => {
          
        const skills = data.allMarkdownRemark.nodes[0].frontmatter.skills || [];
        return (
            <MySkills>
            <div className="columns is-multiline" style={{ padding: "14px 0px" }}>
                {skills.map(function(el) {
                return (
                  <div className="column is-2" key={el.link || el.get('link')} style={{overflow: "auto", textAlign: "center"}}>
    
                    <a href={el.link || el.get('link')} target="_blank" style={{color: "inherit"}} rel="noopener noreferrer">
                      <img className="Sirv" src={el.image || el.get('image') +"-/progressive/yes/-/resize/800x/-/format/auto/"} style={{width: "40%", maxWidth: 150, display: "block", margin: "0 auto", marginBottom: 9, maxHeight: 150}} alt={"image " + el.title } loading="lazy"  />
                      {el.title || el.get('title') }
                    </a>
                  </div>
                )
              })}
            </div>
          </MySkills>
      )}}
    />
  )


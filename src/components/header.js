import { Link } from "gatsby"
// import PropTypes from "prop-types"
import React from "react"
import GithubIcon from "../images/github-icon.svg"
// import styled from "styled-components"
// import AniLink from "gatsby-plugin-transition-link/AniLink"

class Header extends React.Component {
  constructor(props) {
    super()
    this.state = {}
  }
  toggleActive() {
    document.querySelectorAll(".navbar-burger")[0].classList.toggle("is-active")
    document.querySelectorAll(".navbar-menu")[0].classList.toggle("is-active")
  }
  render() {
    return (
      <div className="navbar is-fixed-top">
        <div className="navbar-brand" style={{ marginRight: 16 }}>
          <Link className="navbar-item" to="/">
            samullman.com
          </Link>
          <button
            className="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
            style={{ border: "none" }}
            onClick={this.toggleActive.bind(this)}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
        </div>

        <div className="navbar-menu" style={{ textAlign: "center" }}>
          <div className="navbar-start">
            <a
              className="navbar-item"
              href="https://github.com/samullman/personal"
              target="_blank"
              rel="noopener noreferrer"
            >
              <span className="icon">
                <img src={GithubIcon} alt="twitter icon" />
              </span>
            </a>
          </div>
          <div className="navbar-end">

            {/* <Link className="navbar-item" to="/about">
              animations
            </Link>

            <Link className="navbar-item" to="/about">
              layouts
            </Link> */}

            {/* <Link className="navbar-item" to="/portfolio">
              blog
            </Link> */}

            <Link className="navbar-item" to="/about">
              about me
            </Link>

            <Link className="navbar-item" to="/portfolio">
              portfolio
            </Link>

            <Link className="navbar-item" to="/contact">
              contact
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

export default Header

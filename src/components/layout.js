
import React from "react"
import PropTypes from "prop-types"
import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql, Link } from "gatsby"

import Header from "./header"
import "./layout.scss"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
    <Helmet>
      <html lang="en" className="has-navbar-fixed-top" />
      <link
          rel="icon"
          type="image/png"
          href={`https://img.icons8.com/cute-clipart/64/000000/paint.png`}
          sizes="32x32"
        />
        <link
          rel="icon"
          type="image/png"
          
          href={`https://img.icons8.com/cute-clipart/64/000000/paint.png`}
          sizes="16x16"
        />
    </Helmet>
      <Header siteTitle={data.site.siteMetadata.title} />
      <main>{children}</main>
      {/* <footer className="footer" style={{ padding: "3rem 1.5rem 3rem", background: "rgba(230, 232, 233, 0.5)" }}>
          <div className="content has-text-centered">
              <Link to="/" style={{color: "inherit"}}>samullman.com</Link>
          </div>
          © {new Date().getFullYear()}, Built with {` `} <a href="https://www.gatsbyjs.org">Gatsby</a>
      </footer> */}
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

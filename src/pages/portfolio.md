---
title: Portfolio Page
templateKey: portfolio-page
items:
- attributes:
  - attribute: gatsby
  - attribute: react
  - attribute: netlify-cms
  - attribute: styled-components
  - attribute: netlify
  desc: Redefining edibles with microencapsulation.
  images:
  - image: https://ucarecdn.com/1d48d48a-5449-4062-8540-1ed02e934b4b/
  - image: https://ucarecdn.com/4dfcd47f-c991-4086-8df5-5561ea831658/
  - image: https://ucarecdn.com/aee3b2fa-63ff-49ee-8709-69ad97c3781a/
  - image: https://ucarecdn.com/cb542a84-f2e6-4489-881e-55406b981a7c/
  link: https://1906newhighs.com/
  title: '1906'
- attributes:
  - attribute: gatsby
  - attribute: styled-components
  - attribute: snipcart
  - attribute: amazon
  - attribute: shopify
  desc: 'A coffee roasting business. '
  images:
  - image: https://ucarecdn.com/8b9607ee-3bed-49d5-b1ef-964ac606b591/
  - image: https://ucarecdn.com/d01b9cbd-07fc-42cb-a6ef-ed242b54ef4f/
  - image: https://ucarecdn.com/4f119590-df4d-4b7d-9a52-fc9d2b080758/
  - image: https://ucarecdn.com/63bf2a40-c775-4b60-9af7-57c3dbc51d63/
  link: https://meancoffee.co
  title: Mean Coffee Company
- attributes:
  - attribute: gatsby
  - attribute: react
  - attribute: netlify-cms
  - attribute: styled-components
  - attribute: netlify
  - attribute: google-sheets
  desc: 'Forward looking job board. '
  images:
  - image: https://ucarecdn.com/e36b4b6a-29ed-485b-a9fa-b163f6cb810d/
  link: https://impact-hunt.netlify.com
  title: Impact Hunt
- attributes:
  - attribute: gatsby
  - attribute: react
  - attribute: netlify-cms
  - attribute: styled-components
  - attribute: netlify
  desc: Mock school district website.
  images:
  - image: https://ucarecdn.com/2d272888-e683-4c0c-8f1e-bd35873c8715/
  - image: https://ucarecdn.com/bba4d212-2401-47f1-bc04-a7e4f1497b79/
  - image: https://ucarecdn.com/45ce5500-c034-4719-9938-8f04548a8c1f/
  - image: https://ucarecdn.com/3a8393a8-debf-4b18-a1d6-41200f29f08d/
  link: https://koala-creek.netlify.com/
  title: Koala Creek School District
- attributes:
  - attribute: react
  - attribute: ruby-on-rails
  - attribute: j-query
  - attribute: heroku
  desc: Overseeing the largest economic corridor in Oregon and SW Washington.
  images:
  - image: https://ucarecdn.com/702e31bd-602a-45b0-be79-6f940a275476/
  - image: https://ucarecdn.com/3cdd28a1-8505-40c0-b712-f7076d31c55c/
  - image: https://ucarecdn.com/3c61cd18-3905-457d-9c1f-c30e182a6b85/
  link: http://cca.works
  title: CCA
---


import React from 'react'
import Layout from '../../components/layout'

export default () => (
  <Layout>
    <section className="section">
      <div className="container">
        <div className="content has-text-centered" style={{minHeight: "40vh"}}>
          <h1>thank you!</h1>
        </div>
      </div>
    </section>
  </Layout>
)


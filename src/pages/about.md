---
title: About Page
templateKey: about-page
skills:
- image: https://ucarecdn.com/d9277e58-624c-4c0d-aea0-a80f6fdf41f6/
  link: https://www.gatsbyjs.org/
  title: Gatsby
- image: https://ucarecdn.com/0c944ddf-cb9b-4746-9073-8cbc6645e445/
  link: https://reactjs.org/
  title: React.js
- image: https://ucarecdn.com/29c7a87b-1386-4233-91dc-3942fc520e31/
  link: https://www.styled-components.com/
  title: Styled Components
- image: https://ucarecdn.com/baae5647-8af5-4799-8d9f-fa7ea086aac7/
  link: https://www.netlify.com/
  title: Netlify
- image: https://ucarecdn.com/6fdba9ad-533b-479d-9789-9e4f55fb2f55/
  link: https://github.com/
  title: Github
- image: https://ucarecdn.com/a51ccbf6-7ef6-4234-b339-303758a8877a/
  link: https://firebase.google.com
  title: Firebase
- image: https://ucarecdn.com/0bc41c50-4750-41fb-844f-d97dad7b23a7/
  link: https://www.vuejs.org
  title: Vue
- image: https://ucarecdn.com/f86e0332-d3ed-4b3d-8304-34b472a19fb7/
  link: https://stripe.com/
  title: Stripe
- image: https://ucarecdn.com/751265a0-cfac-4e4e-8024-f90a54641e7b/
  link: https://nodejs.org/
  title: Node
- image: https://ucarecdn.com/7906c47f-5d3b-47b5-837f-2fd4bbe5425e/
  link: https://heroku.com
  title: Heroku
- image: https://ucarecdn.com/45ed118b-150e-4a1e-bc7d-ac2a7e36f0bc/
  link: https://www.shopify.com/
  title: Shopify
- image: https://ucarecdn.com/8e8f3f19-cc8f-4a2e-b346-ae07b8d21964/
  link: https://rubyonrails.org/
  title: Ruby on Rails
items:
- date: '2012'
  desc: |-
    * Liberal-arts college based in Southern California.
    * Science & Management Major.
  image: https://ucarecdn.com/f1830572-d93b-4260-9990-c1fe0cf160f3/
  link: https://www.cmc.edu/
  title: Claremont McKenna College
- date: '2015'
  desc: |-
    * Web fundamentals bootcamp based in San Francisco, CA.
    * Algorithms in Ruby and Javascript.
  image: https://ucarecdn.com/e282e525-4cc3-4b10-9ba5-022a5e7f9d22/
  link: https://www.appacademy.io/
  title: App Academy
---


import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
// import Img from "gatsby-image"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"
import jump from "jump.js"

// import {IntlProvider, FormattedMessage} from 'react-intl';

const Title = styled.div`
  color: #4a4a4a;
  font-size: 2em;
  font-weight: 600;
  text-align: center;
  margin-bottom: 8px;
`


const VideoTitle = styled.div`
  position: absolute;
  text-align: center;
  width: 100%;
  font-size: 44px;
  letter-spacing: 4px;
  padding-top: 20vw;

  @media screen and (max-width: 768px) {
    font-size: 32px;
  }

  @media screen and (max-width: 468px) {
    font-size: 24px;
  }

  div {
    color: white;
  }
`

const JumpButton = styled.button`
  background: none;
  border: none;
  padding: 2px;
  font-size: 24px;
  font-family: inherit;
  color: inherit;
  cursor: pointer;
  outline: none;
  transition: color 0.2s ease;
  

  &:hover {
    color: #6067c0;    
  }
`

const PoweredByWrapper = styled.div`
// background-image: linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%);
`
const PoweredByContainer = styled.div`
  display: block;
  max-width: 1080px;
  margin: 0 auto;
  padding: 10vh 20px;
  
`

export default class Page extends React.Component {
  constructor(props) {
    super()
    this.state = {}
    this.state.poweredBy =
      props.data.allMarkdownRemark.nodes[0].frontmatter.poweredBy || []
      
    this.state.wordpress = props.data.allWordpressPost.edges[0].node.content;
    this.state.contentful = "";
    this.state.contentfulData = props.data.allContentfulBlog.edges[0].node.body.content;
    
  }

  contentCallback ( el ) {
    el.content.forEach( function ( innerEl ) {
      if ( innerEl.value ) {
        this.state.contentful += innerEl.value
      }

      if ( innerEl.content ) {
        let contentString = "<a target=\"_blank\" href=\"" + innerEl.data.uri + "\">" + innerEl.content[0].value + "</a>"; 
        this.state.contentful += contentString;
      }

      this.setState({
        contentful: this.state.contentful
      })
    }.bind(this))
  }

  componentDidMount () {
    this.state.contentfulData.forEach( this.contentCallback.bind(this) )
  }

  poweredBy(el) {
    return (
      <div id={el.name} key={el.name} className="column is-4" style={{padding: 14}}>
        <div className="card">
        <div className="card-image" style={{padding: "20px 30px"}}>
          <a href={el.link} target="_blank">
            <img
              className="Sirv"
              src={el.logo + "#/progressive/yes/"}
              alt={el.name + " logo"}
              loading="lazy"
            />
          </a>
        </div>

        <div className="notification" dangerouslySetInnerHTML={{__html: this.state[el.name]}} style={{marginBottom: 0}}>
        </div>

        <div className="card-content">
        <Link to="/">Read more ></Link>
        </div>
        </div>
      </div>
    )
  }
  render() {
    return (
      <Layout>
        <SEO title="home" />

        <VideoTitle>
          <div>DEVELOPING THE</div>
          <div>MODERN WEB</div>
        </VideoTitle>

        <video
          autoPlay
          muted
          loop
          style={{ display: "block", margin: "0 auto", minHeight: "56vw" }}
          alt="video globe"
          loading="lazy"
        >
          <source
            src="https://res.cloudinary.com/djjldnjz7/video/upload/v1560384827/Komp_1_zrsapg.mp4"
            type="video/mp4"
          />
        </video>

        {/* <PoweredByWrapper>
        <PoweredByContainer>
          <div style={{textAlign: "center"}}>
            this website is
          </div>
          <Title>powered by:</Title>

          <div className="has-text-centered" style={{marginTop: 20, marginBottom: 60 }}>
            {this.state.poweredBy.map(el => {
              return (
                <span style={{ marginRight: 8 }}>
                  <JumpButton onClick={() => jump("#" + el.name)} >
                    {el.name}
                  </JumpButton>
                  ,
                </span>
              )
            })}
          </div>

        <div className="columns is-multiline">

        {this.state.poweredBy.map(el => this.poweredBy(el))}
        </div>
        </PoweredByContainer>
        </PoweredByWrapper> */}
      </Layout>
    )
  }
}

export const query = graphql`
  {
    allMarkdownRemark(
      filter: { frontmatter: { templateKey: { eq: "home-page" } } }
    ) {
      nodes {
        frontmatter {
          poweredBy {
            logo
            name
            link
          }
        }
      }
    }

    allWordpressPost(filter: { title: { eq: "Hello" } }) {
      edges {
        node {
          content
        }
      }
    }

    allContentfulBlog {
      edges {
        node {
          body {
            body
            content {
              content {
                value
                content {
                  value
                }
                data {
                  uri
                }
              }
            }
          }
        }
      }
    }
  }
`

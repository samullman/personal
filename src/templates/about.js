import React from "react"
import { Link, graphql  } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"
import ReactMarkdown from "react-markdown";
import Skills from "../components/skills.js"
const AboutSection = styled.div `
  @media screen and (max-width: 768px) {
    text-align: center;
  }
`

const AboutTitle = styled.div `
  font-size: 2rem;
  font-weight: 600;

  @media screen and (max-width: 768px) {
    text-align: center;
  }
`
const AboutDesc = styled.div `
  margin-top: 5px;
  margin-bottom: 12px;

  ul {
    margin-left: 1.5em;
    margin-top: .5em;

    @media screen and (max-width: 768px) {
      li {
        list-style: none;
      }
    }
    
    
  }
`

const AboutDate = styled.div `
  font-style: italic;
`

export default (props) => {
  const items = props.data.allMarkdownRemark.nodes[0].frontmatter.items || [];
  
  return (
    <Layout>
      <SEO title="about" />

      <h1 className="title is-1 has-text-centered">
        About
      </h1>

      <div className="container" style={{maxWidth: 840}}>
          {
              items.map( (el) => {
                return (
                  <AboutSection key={ el.link } style={{marginBottom: 24}}>
                    <div className="columns">
                      <div className="column is-4">
                        <a href={ el.link } target="_blank" rel="noopener noreferrer">
                          <img className="Sirv" src={el.image + "-/progressive/yes/-/resize/800x/-/format/auto/" } alt={"image " + el.title } loading="lazy"></img>
                        </a>
                      </div>

                      <div className="column is-8">
                      
                      <AboutTitle>
                        { el.title }
                      </AboutTitle>

                      <AboutDesc>
                        <div className="content">

                        <ReactMarkdown source={ el.desc } />
                        </div>

                      </AboutDesc>

                      <AboutDate>
                        { el.date }
                      </AboutDate>

                      </div>
                    </div>
                  </AboutSection>
                )
              })
            }
      </div>

      <div className="container" style={{ maxWidth: 840 }}>
        <ToolsIUse />
      </div>
      

      <div className="has-text-centered" style={{padding: "20px 0px", marginBottom: 40}}>
        <a href="https://res.cloudinary.com/djjldnjz7/image/upload/v1566068802/cv_tvxp7g.pdf">
            Resume
        </a>
      </div>
    </Layout>
  );
};

const ToolsIUse = () => (
  <div>
    <AboutTitle>
      Tools I Use:
    </AboutTitle>

    <Skills />
  </div>
)


export const query = graphql`
{
  allMarkdownRemark(filter: {frontmatter: {templateKey: {eq: "about-page"}}}) {
    nodes {
      frontmatter {
        title
        templateKey
        items {
          image
          link
          title
          desc
          date
        }
      }
    }
  }
}

`

import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"

const isBrowser = typeof window !== 'undefined';
const Flickity = isBrowser ? require('react-flickity-component') : 'div';




const PortfolioItem = styled.div`
    margin-bottom: 142px;
`

const SlideShow = styled.div`
  width: 300px;
  height: 200px;
  outline: none;
  display: block;
  margin: 0 auto;
  margin-bottom: 20px;

  div {
    outline: none;
  }

  .flickity-viewport {
    overflow: hidden;
  }

  .flickity-button.previous {
    position: relative;
    top: -117px;
    left: -44px;
  }

  .flickity-button.next {
    position: relative;
    top: -117px;
    left: 274px;
  }

  .flickity-button {
    display: none;
    background: none;
    border: none;
    color: grey;
    width: 34px;
    height: 34px;
    border-radius: 50%;
    cursor: pointer;

    svg {
      fill: #363636;
    }
  }

  .flickity-page-dots {
    list-style: none;
    display: block;
    margin: 0 auto;
    text-align: center;
    


    .dot {
      display: inline-block;
      width: 10px;
      height: 10px;
      margin: 0 8px;
      background: #333;
      border-radius: 50%;
      opacity: 0.25;
      cursor: pointer;

      &:hover {
        opacity: 0.5;
      }

      &.is-selected {
        opacity: 1;
      }
    }
  }
`

const PortfolioImage = styled.div`
  background-size: cover !important;
  height: 200px;
  width: 300px;
  margin-right: 20px;
`

const PortfolioContent = styled.div`
    text-align: center;
    max-width: 460px;
    margin: 0 auto;
    padding: 12px;
`

const PortfolioAttributes = styled.div`
  text-align: center;
  margin-bottom: 14px;
`

const PortfolioDesc = styled.div`
`

export default props => {
  const items = props.data.allMarkdownRemark.nodes[0].frontmatter.items || []
  const attributes = {
    gatsby: {
      title: "Gatsby",
      link: "https://www.gatsbyjs.org/",
    },
    react: {
      title: "React.js",
      link: "https://reactjs.org/",
    },
    "netlify-cms": {
      title: "Netlify CMS",
      link: "https://www.netlifycms.org/",
    },
    "styled-components": {
      title: "Styled Components",
      link: "https://www.styled-components.com/",
    },
    netlify: {
      title: "Netlify",
      link: "https://www.netlify.com/",
    },
    "google-sheets": {
      title: "Google Sheets",
      link: "https://www.gatsbyjs.org/packages/gatsby-source-google-sheets/",
    },
    "ruby-on-rails": {
      title: "Ruby On Rails",
      link: "https://rubyonrails.org/",
    },
    heroku: {
      title: "Heroku",
      link: "https://www.heroku.com",
    },
    "j-query": {
      title: "jQuery",
      link: "https://jquery.com/",
    },
    shopify: {
      title: "Shopify",
      link: "https://shopify.com/",
    },

    snipcart: {
      title: "Snipcart.js",
      link: "https://snipcart.com/",
    },

    amazon: {
      title: "Amazon FBA",
      link: "https://www.amazon.com/",
    },
  }

  return (
    <Layout>
      <SEO title="portfolio" />
      <h1 className="title is-1 has-text-centered">
        Portfolio
      </h1>
      {items.map(el => {
        
        return (
          <PortfolioItem key={el.image}>
            
            <SlideShow>
              <Flickity options={{wrapAround: true, watchCSS: false}}>
              {el.images.map(image => {
                return (
                  <a href={el.link} target="_blank" rel="noopener noreferrer" key={image.image}>
                  <PortfolioImage
                    style={{
                      background:
                        "url(" + image.image + "-/progressive/yes/-/resize/600x/-/format/auto/)",
                    }}
                    
                  ></PortfolioImage>
                  </a>
                )
              })}
              </Flickity>
            </SlideShow>
            
            
            <PortfolioContent>
            <h4 className="title has-text-centered" style={{marginBottom: 12}}>{el.title}</h4>

            <PortfolioAttributes>
              {el.attributes.map(attribute => {
                  
                attribute = attributes[ attribute.attribute ];

                let key =
                  Math.random()
                    .toString(36)
                    .substring(2, 15) +
                  Math.random()
                    .toString(36)
                    .substring(2, 15)
                return (
                  <a href={attribute.link} target="_blank" rel="noopener noreferrer" key={key}>
                    <div
                      style={{
                        padding: "5px 16px",
                        borderRadius: 13,
                        background: "#d3d3d3",
                        display: "inline-block",
                        margin: 4,
                      }}
                    >
                      {attribute.title}
                    </div>
                  </a>
                )
              })}
            </PortfolioAttributes>

            <PortfolioDesc>
                {
                    el.desc
                }
            </PortfolioDesc>
            </PortfolioContent>
          </PortfolioItem>
        )
      })}
    </Layout>
  )
}

export const query = graphql`
  {
    allMarkdownRemark(
      filter: { frontmatter: { templateKey: { eq: "portfolio-page" } } }
    ) {
      nodes {
        frontmatter {
          title
          templateKey
          items {
            images {
              image
            }
            attributes {
                attribute
            }
            link
            title
            desc
            date
          }
        }
      }
    }
  }
`

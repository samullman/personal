module.exports = {
  siteMetadata: {
    title: `samullman`,
    description: `Personal site.`,
    author: `samullman`,
  },
  plugins: [
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },

    `gatsby-plugin-react-helmet`,

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },
    
    `gatsby-transformer-remark`,
    
    'gatsby-plugin-sass',
    `gatsby-plugin-transition-link`,
    `gatsby-plugin-styled-components`, 
    {
      resolve: 'gatsby-plugin-netlify-cms',
      options: {
        modulePath: `${__dirname}/src/cms.js`,
      },
    },
    // {
    //   resolve: `gatsby-source-contentful`,
    //   options: {
    //     spaceId: "",
    //     accessToken: "",
    //   },
    // },
    {
      resolve: "gatsby-source-wordpress",
      options: {
        baseUrl: "samullman.wordpress.com",
        protocol: "http",
        hostingWPCOM: true,
        useACF: false,
        includedRoutes: [
          "**/posts",
          "**/pages",
        ],

      }
    },

    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `st2pvonhrepj`,
        // Learn about environment variables: https://gatsby.dev/env-vars
        accessToken: process.env.GATSBY_CONTENTFUL_ACCESS_TOKEN,
      },
    },

    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/globe-icon.svg`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
